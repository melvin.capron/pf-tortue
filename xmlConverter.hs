module XmlConverter where

createXmlTag :: String
createXmlTag = "<?xml version =\"1.0\" encoding=\"utf-8\"?>"

exportToXml :: String -> String
exportToXml content = createXmlTag ++ content