import Prelude
import Screen
import Turtle
import Instructions
import Shape
import Image
import Order

getBaseScreen :: Screen
getBaseScreen = (Screen 500 500 [])

getBaseTurtle :: Screen -> Turtle
getBaseTurtle (Screen x y _) = (Turtle (x/2) (y/2) 0 True)

main :: IO ()
main = let
         (screen1, turtle1) = move(getBaseScreen, getBaseTurtle(getBaseScreen)) 30 "green"
         (screen2, turtle2) = move((turnFromCurrent(screen1, turtle1)) 90) 30 "yellow"
         (screen3, turtle3) = move((turnFromCurrent(screen2, turtle2)) 90) 30 "red"
         (screen4, turtle4) = move((turnFromCurrent(screen3, turtle3)) 90) 30 "blue"

         (screenSquare, turtleSquare) = drawSquare(getBaseScreen, getBaseTurtle(getBaseScreen))
         (screenPolygon, turtlePolygon) = drawPolygon(getBaseScreen, getBaseTurtle(getBaseScreen))
         (screenMill, turtleMill) = drawMill(getBaseScreen, getBaseTurtle(getBaseScreen)) 0

         (screenVonKochOrder, turtleVonKochOrder) = drawVonKochSnowflake(getBaseScreen, getBaseTurtle(getBaseScreen)) 3 150 3
       in do
         export "examples/test.svg" (createScreenXml screen4 ++ buildScreenShapes(getShapes(screen4)))
         export "examples/test_square.svg" (createScreenXml screenSquare ++ buildScreenShapes(getShapes(screenSquare)))
         export "examples/test_polygon.svg" (createScreenXml screenPolygon ++ buildScreenShapes(getShapes(screenPolygon)))
         export "examples/test_mill.svg" (createScreenXml screenMill ++ buildScreenShapes(getShapes(screenMill)))
         export "examples/test_von_koch_order.svg" (createScreenXml screenVonKochOrder ++ buildScreenShapes(getShapes(screenVonKochOrder)))
         print $ turtle2

