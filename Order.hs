module Order where

data Order = MoveForward | MoveBackward | MoveLeft | MoveRight | TurnBack | TurnLeft | TurnRight | Repeat [(Order, Double)] deriving (Show)