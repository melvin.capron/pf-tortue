module Turtle where

data Turtle =
    Turtle {
        x :: Double,
        y :: Double,
        angle :: Double,
        isWriting :: Bool
    } deriving (Show)

lowerPen :: Turtle -> Turtle
lowerPen (Turtle x y angle isWriting) = Turtle x y angle True

upperPen :: Turtle -> Turtle
upperPen (Turtle x y angle isWriting) = Turtle x y angle False
