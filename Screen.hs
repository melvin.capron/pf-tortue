module Screen where

import Shape

data Screen =
    Screen {
        widthScreen :: Double,
        heightScreen :: Double,
        shapes :: [Shape]
    } deriving (Show)

getShapes :: Screen -> [Shape]
getShapes (Screen x y shapes) = shapes