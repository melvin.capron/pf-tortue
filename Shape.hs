module Shape where

data Shape =
    Rectangle {
        x :: Double,
        y :: Double,
        widthRec :: Double,
        heightRec :: Double
    }
    | Circle {
        cx :: Double,
        cy :: Double,
        r :: Double
    }
    | Line {
        x1 :: Double,
        y1 :: Double,
        x2 :: Double,
        y2 :: Double,
        color :: String
    } deriving (Show)