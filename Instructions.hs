module Instructions where

import Prelude
import Turtle
import Image
import Screen
import Shape
import Order

turn :: (Screen, Turtle) -> Double -> (Screen, Turtle)
turn ((Screen sX sY shapes), (Turtle tX tY angle isWriting)) nAngle = ((Screen sX sY shapes), (Turtle tX tY nAngle isWriting))

turnFromCurrent :: (Screen, Turtle) -> Double -> (Screen, Turtle)
turnFromCurrent ((Screen sX sY shapes), (Turtle tX tY angle isWriting)) nAngle = ((Screen sX sY shapes), (Turtle tX tY (angle + nAngle) isWriting))

move :: (Screen, Turtle) -> Double -> String -> (Screen, Turtle)
move ((Screen sX sY shapes), (Turtle tX tY angle isWriting)) dist color = ((Screen sX sY (nL:shapes)), (Turtle nX nY angle isWriting))
    where
     nX = tX + dist * cos(angle*(pi/180))
     nY = tY + dist * sin(angle*(pi/180))
     nL = (Line tX tY nX nY color)

-- Ask Mr Verel about Maybe Monad ? Possible here?
--drawSquare :: (Screen, Turtle) -> Maybe Int -> (Screen, Turtle)
--drawSquare (screen, turtle) Nothing = drawSquare(screen, turtle) 1
--drawSquare :: (Screen, Turtle) -> Int -> (Screen, Turtle)
--drawSquare :: (Screen, Turtle) -> (Screen, Turtle)
--drawSquare (screen, turtle) = repeatOrder (screen, turtle) 4 [("move", 30), ("turn", 90)]

drawSquare :: (Screen, Turtle) -> (Screen, Turtle)
drawSquare (screen, turtle) = executeMultiple (screen, turtle) [(Repeat [(MoveForward, 30), (TurnRight, 0)], 4)]

drawPolygon :: (Screen, Turtle) -> (Screen, Turtle)
drawPolygon (screen, turtle) = executeMultiple (screen, turtle) [(Repeat [(MoveForward, 30), (TurnRight, 45)], 8)]

drawMill :: (Screen, Turtle) -> Int -> (Screen, Turtle)
drawMill (screen, turtle) 4 = (screen, turtle)
drawMill (screen, turtle) 0 = drawMill(drawSquare(executeMultiple (screen, turtle) [(TurnRight, 45), (MoveForward, 45)])) (1)
drawMill (screen, turtle) it = drawMill(drawSquare(executeMultiple (screen, turtle) [(TurnBack, 0), (MoveForward, 25), (TurnRight, 0), (MoveForward, 25)])) (it + 1)

-- Turning without writing
drawVonKochPattern :: (Screen, Turtle) -> Int -> Double -> (Screen, Turtle)
drawVonKochPattern (screen, turtle) 0 size = move(screen, turtle) size "green"
drawVonKochPattern (screen, turtle) it size = drawVonKochPattern (turnFromCurrent (drawVonKochPattern (turnFromCurrent (drawVonKochPattern (turnFromCurrent (drawVonKochPattern (screen, turtle) (it-1) (finalSize)) 300) (it-1) (finalSize)) 120) (it-1) (finalSize)) 300) (it-1) (finalSize)
    where
        finalSize = size/3

drawVonKochSnowflake :: (Screen, Turtle) -> Int -> Double -> Int -> (Screen, Turtle)
drawVonKochSnowflake (screen, turtle) it size 0 = (screen, turtle)
drawVonKochSnowflake (screen, turtle) it size r = drawVonKochSnowflake (turnFromCurrent(drawVonKochPattern(screen, turtle) it size) 120) it size (r-1)

execute :: (Screen, Turtle) -> Order -> Double -> (Screen, Turtle)
execute (screen, turtle) MoveForward dist = move (screen, turtle) dist "red"
execute (screen, turtle) MoveBackward dist = move (turnFromCurrent(screen, turtle) 180) dist "red"
execute (screen, turtle) MoveLeft dist = move (turnFromCurrent(screen, turtle) 270) dist "yellow"
execute (screen, turtle) MoveRight dist = move (turnFromCurrent(screen, turtle) 90) dist "red"
execute (screen, turtle) TurnRight angle = turnFromCurrent(screen, turtle) (90 - angle)
execute (screen, turtle) TurnLeft angle = turnFromCurrent(screen, turtle) (270 - angle)
execute (screen, turtle) TurnBack angle = turnFromCurrent(screen, turtle) (180 - angle)
execute (screen, turtle) (Repeat orders) num = repeatOrder (screen, turtle) num orders

executeMultiple :: (Screen, Turtle) -> [(Order, Double)] -> (Screen, Turtle)
executeMultiple (screen, turtle) [] = (screen, turtle)
executeMultiple (screen, turtle) ((k, v):t) = executeMultiple (execute (screen, turtle) k v) t

repeatOrder :: (Screen, Turtle) -> Double -> [(Order, Double)] -> (Screen, Turtle)
repeatOrder (screen, turtle) 0 _ = (screen, turtle)
repeatOrder (screen, turtle) repeatIt arr = repeatOrder (executeMultiple(screen, turtle) arr) (repeatIt-1) arr