module Image where

import XmlConverter
import System.IO
import Data.Char
import Shape
import Screen

-- Ouverture de la balise svg
createScreenXml :: Screen -> String
createScreenXml (Screen widthScreen heightScreen _) = "<svg viewBox=\"0 0 " ++ (show widthScreen) ++ " " ++ (show heightScreen) ++ "\" xmlns=\"http://www.w3.org/2000/svg\">"


-- Boucle sur le tableau de formes pour remplir le svg
buildScreenShapes :: [Shape] -> String
buildScreenShapes [] = "</svg>" -- Fermeture de la balise svg Ã  la fin du tableau
buildScreenShapes (shapesH:shapesT) = (createShapeXml shapesH) ++ (buildScreenShapes shapesT)

endShapeSvg :: String -> String
endShapeSvg content = content ++ "</svg>"

createShapeXml :: Shape -> String
createShapeXml (Rectangle x y widthRec heightRec) = "<rect x=\"" ++ (show x) ++ "\" y=\"" ++ (show y) ++ "\" width=\"" ++ (show widthRec) ++ "\" height=\"" ++ (show heightRec) ++ "\"/>"
createShapeXml (Circle cx cy r) = "<circle cx=\"" ++ (show cx) ++ "\" cy=\"" ++ (show cy) ++ "\" r=\"" ++ (show r) ++ "\" />"
createShapeXml (Line x1 y1 x2 y2 color) =  "<line x1=\"" ++ (show x1) ++ "\" y1=\"" ++ (show y1) ++"\" x2=\"" ++ (show x2) ++ "\" y2=\"" ++ (show y2) ++ "\" stroke=" ++ (show color) ++ "/>"

export :: FilePath -> [Char] -> IO ()
export filename content = writeFile filename (createXmlTag ++ content)
